const Web3 = require('web3')

const rpcUrl = "https://mainnet.infura.io/v3/726abb882b004191bc86787f466c710f"

const web3 = new Web3(rpcUrl)

const account = "0xeD33259a056F4fb449FFB7B7E2eCB43a9B5685Bf"
async function displayBalance() {
    let wei = await web3.eth.getBalance(account)

    let balance = web3.utils.fromWei(wei, 'ether')
    console.log(balance)
}
displayBalance();


let newWallet = web3.eth.accounts.create()
// > {
//    address: "0xb8CE9ab6943e0eCED004cDe8e3bBed6568B2Fa01",
//    privateKey: "0x348ce564d427a3311b6536bbcff9390d69395b06ed6c486954e971d960fe8709",
//    signTransaction: function(tx){...},
//    sign: function(data){...},
//    encrypt: function(password){...}
// }

console.log(newWallet)